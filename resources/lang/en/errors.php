<?php

return [
    'default' => [
        'message' => 'Internal server error',
    ],

    'method_not_allowed_http_exception' => [
        'message' => 'Method not allowed',
    ],

    'not_found_http_exception' => [
        'message' => 'Not found',
    ],

    'throttle_requests_exception' => [
        'message' => 'An error occurred, please try later',
    ],

    'model_not_found_exception' => [
        'message' => 'Not found',
    ],

    'query_exception' => [
        'message' => 'Ошибка выполнения запроса',
    ],

    'validation_exception' => [
        'message' => 'Invalid data',
    ],

    'unauthorized_exception' => [
        'message' => 'Unauthorized',
    ],

    'authentication_exception' => [
        'message' => 'Unauthenticated.',
    ]

];
