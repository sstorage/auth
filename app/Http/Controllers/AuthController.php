<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Jobs\UpdateStatsAfterUserUpdate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\UnauthorizedException;

class AuthController extends Controller
{
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->get('data');

        if (! $token = auth()->attempt($credentials)) {
            $e = new UnauthorizedException('Token mismatch');
            return response()->sendError($e, 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * User registration
     */
    public function registration(RegistrationRequest $request)
    {
        $userData = $request->get('data');

        $user = new User($userData);
        $user->uuid = \Str::uuid();
        $user->save();

        return response()->sendSuccess(['message' => 'Successfully registration!']);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userInfo()
    {
        $user = auth()->user();
        return response()->sendSuccess($user->toArray());
    }

    public function update(UpdateUserRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $userData = $request->get('data');
        $user->fill($userData);
        $user->save();

        UpdateStatsAfterUserUpdate::dispatch($user->toArray());

        return response()->sendSuccess($user->toArray());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->sendSuccess(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function verify()
    {
        auth()->check();
        return response()->sendSuccess(['message' => 'Verified']);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->sendSuccess([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
