<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.name' => 'required',
            'data.lastname' => 'required',
            'data.email' => 'required|email',
            'data.password' => 'required',
        ];
    }
}
